<!-- Por favor verifique primero si de verdad se trata de una solicitud de
mejora.

Se entiende que una solicitud es de mejora si el sistema funcionalmente hace lo
que se supone que tiene que hacer, sin embargo se puede hacer más rápido o
presentar información más clara, cambiar colores y estilos, entre otros, para
tener una idea verifique si la solicitud se trata de:

* Cambiar un texto por otro más claro para el usuario.
* Modificar los colores para que sea más coherente y consistente con el sistema
  y con los definidos en el manual corporativo de marca.
* Eliminar un texto.
* Modificar parámetros de comportamiento del sistema (Condiciones).
* Eliminación de elementos cuya presencia entorpece la experiencia de usuario.
* Entre otros.

Si solicitud no cumple con estas características quizá su solicitud es de una
*Nueva Funcionalidad*. -->

<!-- Cambie el espacio entre los corchetes por una equis así: `[x]` -->
* [ ] Estoy seguro de que esto es una solicitud de mejora.

# Descripción de la solicitud

<!-- Describa brevemente su solicitud -->

## Estado actual

<!-- Cuál es el estado actual del inconveniente que requiere ser cambiado? -->

## Estado deseado

<!-- Cómo es el estado de éxito para cumplir con esta solicitud? -->

1. <!-- Primera característica. -->
2. <!-- Segunda característica. -->
<!-- n. Enésima característica. -->

## Información adicional

### URL

<!-- La dirección (URL) donde se debe evidenciar la solución. -->

### Evidencias adjuntas

<!-- Inserte aquí las vídeo demostraciones o capturas de pantalla. -->

### Observaciones adicionales

<!-- Si tiene alguna observación o apunte que ayude a llegar a una mejor
solución del problema coméntelo aquí. -->

### Módulos afectados

<!-- Si esta mejora afecta otros módulos por favor lístelos a continuación -->

* <!-- Módulo A -->
* <!-- Módulo B -->
* <!-- Módulo C -->

<!-- Por favor no modifique esta sección -->
/label ~New
/label ~Enhancement
