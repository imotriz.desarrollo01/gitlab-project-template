<!-- Por favor verifique primero si de verdad se trata de una solicitud de
nueva funcionalidad.

Se entiende que una solicitud es de nueva funcionalidad es aquella que se
refiere a un comportamiento del sistema que no estaba contemplado anteriormente
o que no se implementó en el proceso de desarrollo, en este tipo tenemos por
ejemplo:

* Añadir un paso más a un proceso de validación.
* Añadir un paso más a la historia de usuario o caso de uso.
* Implementar un campo de entrada nuevo.
* Un nuevo módulo.
* Un nuevo funcionamiento dentro de un módulo conocido.
* Añadir información a una vista o a una salida del sistema.

Si solicitud no cumple con estas características quizá su solicitud es para una
*Mejora del Sistema* -->

<!-- Cambie el espacio entre los corchetes por una equis así: `[x]` -->
* [ ] Estoy seguro de que esto es una solicitud de mejora.

# Descripción de la solicitud

<!-- Describa brevemente su solicitud -->

## Estado deseado

<!-- Cómo es el estado de éxito para cumplir con esta solicitud? -->

1. <!-- Primera característica. -->
2. <!-- Segunda característica. -->
<!-- n. Enésima característica. -->

## Información adicional

### URL

<!-- La dirección (URL) donde se debe evidenciar la solución, se aclara que este
campo es opcional. -->

### Propuestas de diseño

<!-- Inserte aquí las vídeo demostraciones o capturas de pantalla. -->

### Observaciones adicionales

<!-- Si tiene alguna observación o apunte que ayude a llegar a una mejor
solución del problema coméntelo aquí. -->

<!-- Por favor no modifique esta sección -->
## Seguimiento

* [ ] Solución discutida.
* [ ] Solución diseñada.
* [ ] Solución planeada.

/label ~New
/label ~Feature
